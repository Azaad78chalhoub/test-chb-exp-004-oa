connection: "exp-chb-exp-004"

include: "/views/*.view.lkml"                # include all views in the views/ folder in this project
# include: "/**/*.view.lkml"                 # include all views in this project
# include: "my_dashboard.dashboard.lookml"   # include a LookML dashboard called my_dashboard

# # Select the views that should be a part of this model,
# # and define the joins that connect them together.
#
# explore: order_items {
#   join: orders {
#     relationship: many_to_one
#     sql_on: ${orders.id} = ${order_items.order_id} ;;
#   }
#
#   join: users {
#     relationship: many_to_one
#     sql_on: ${users.id} = ${orders.user_id} ;;
#   }
# }

explore: lr_u3_c6_w1660690827858_brand_vertical_mapping_log {
  label: "LR U3 C6"
  view_name: lr_u3_c6_w1660690827858_brand_vertical_mapping_log
}
explore: connection_reg_r3 {
  label: "Connection Reg R3"
  view_name: connection_reg_r3
}

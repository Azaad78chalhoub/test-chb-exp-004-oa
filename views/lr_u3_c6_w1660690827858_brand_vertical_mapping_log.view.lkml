view: lr_u3_c6_w1660690827858_brand_vertical_mapping_log {
  sql_table_name: `chb-exp-004.looker_pdt.LR_U3C6W1660690827858_brand_vertical_mapping_log`
    ;;

  dimension: brand {
    type: string
    sql: ${TABLE}.brand ;;
  }

  dimension: vertical {
    type: string
    sql: ${TABLE}.vertical ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
